
var request = require('request')

// http://developer.echonest.com/api/v4/song/search?api_key=FILDTEOIK2HBORODV&format=json&artist=Virt&bucket=id:fma&bucket=tracks

exports.search = function(query, callback) {
  console.log('search')
  if (typeof query !== 'string' || query.length === 0) {
    callback({code:400, response:{status:'error', message:'missing query (q parameter)'}})
  }
  const url = 'https://freemusicarchive.org/api/get/tracks.xml?api_key=61RW8FHVP4C7RGC5'
  const query_string = {q: query, maxResults: 40, fields: 'items(id,volumeInfo(title,artists))'}
  request.get({url: url, qs: query_string}, function(err, res, body) {
    if (err) {
      callback({code:500, response:{status:'error', message:'search failed', data:err}})
    }
    console.log(typeof body)
    const json = JSON.parse(body)
    const items = json.items
    const tracks = items.map(function(element) {
      return {id:element.id, title:element.volumeInfo.title, artists:element.volumeInfo.artists}
    })
    console.log(tracks)
    callback({code:200, response:{status:'success', message:tracks.length+' tracks found', data:tracks}})
  })
}
